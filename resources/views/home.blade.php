@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="/contacts/assign" method="POST">
                <div class="card">
                    <div class="card-header">Assign Zipcodes</div>
                    <div class="card-body">
                        @if (session('message'))
                            <div class="alert alert-success" role="alert">
                                {{ session('message') }}
                            </div>
                        @endif
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Agent Id</th>
                                    <th>Zip Code</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <th>{{ $user->id }}</th>
                                    <input type="hidden" name="user[{{ $user->id }}][id]" value="{{ $user->id }}">
                                    <th><input type="text" name="user[{{ $user->id }}][zipcode_id]" id="{{ $user->id }}_zipcode_id" value="{{ $user->zipcode_id }}" placeholder="Zipcode"></th>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary">Assign Contacts</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
