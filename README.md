# Zoefin Test Apps's README

This app is a Login App and User Management.

[zoefin Test App](http://127.0.0.1:8000/)

[Source Code](https://laromicas@bitbucket.org/laromicas/zoefin.git) Bitbucket Public Repository

Current Users to test:

- Agent 1
	- user: *agent1@zoefin.com*
	- password: *test123*
- Agent 2
	- user: *agent2@zoefin.com*
	- password: *test123*

<!-- MarkdownTOC autolink="true" bracket="round" markdown_preview="markdown" -->

- [Zoefin Test Apps's README](#zoefin-test-appss-readme)
	- [Installing](#installing)
		- [Clone the repository](#clone-the-repository)
		- [Installing the dependencies](#installing-the-dependencies)
		- [Create the database and import the starting data](#create-the-database-and-import-the-starting-data)
	- [Executing the tests](#executing-the-tests)
	- [Executing the sample app](#executing-the-sample-app)
		- [Model](#model)
			- [Database](#database)
			- [Model Scripts](#model-scripts)
		- [View](#view)
			- [Template files (blade) (only shown the non default)](#template-files-blade-only-shown-the-non-default)
		- [Controllers](#controllers)

<!-- /MarkdownTOC -->

## Installing

### Clone the repository
```sh
$ git clone https://laromicas@bitbucket.org/laromicas/zoefin.git
```
### Installing the dependencies

Make sure you have composer installed, you also need PHP 7.2+ and some plugins.

Linux
```sh
$ sudo apt-get install composer php php-mysql php-sqlite3 php-zip phpunit php-dom
```

Install the dependencies
```sh
$ cd zoefin
$ composer install
```

Generate the key
```sh
$ cp .env.example .env
$ php artisan key:generate
```

### Create the database and import the starting data

- Modify .env file with the database connection
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=zoefin
DB_USERNAME=root
DB_PASSWORD=root
```
- Execute the migrations
```sh
$ php artisan migrate
```
Import the starting data
```sh
$ php artisan db:seed --class=UsersTableSeeder
$ php artisan db:seed --class=ContactsTableSeeder
$ php artisan db:seed --class=ZipcodesTableSeeder
```

## Executing the tests
```sh
$ phpunit
```
## Executing the sample app
```sh
$ php artisan serve
```
After this you can go to [http://127.0.0.1:8000/](http://127.0.0.1:8000/) to test the app

### Model

#### Database

> - [DML](http://127.0.0.1:8000/database.sql)

Tables

> - **contacts** - Contacts (clients) Table with zipcode and associated agent.
> - **users** - Users (agents) Table with auth data and zipcode.
> - **zipcodes** - Table with all the zip codes in the USA (downloaded from [here](https://www2.census.gov/geo/docs/maps-data/data/gazetteer/2014_Gazetteer/2014_Gaz_zcta_national.zip))

#### Model Scripts

> - **app/User.php** - Model for Users Table
> - **app/Contact.php** - Model for Contacts Table
> - **app/Zipcode.php** - Model for Zipcodes Table

### View

#### Template files (blade) (only shown the non default)
> - **resources/views/home.blade.php** - Shows the form to change agents zipcodes
> - **resources/views/list.blade.php** - Shows the assigned contacts to each agent

### Controllers

> - **app\Http\Controllers\Auth\LoginController.php** - Handles the user's login, logout, can handle api requests
> 	- *functions:*
> 		- *login, logout, apiLogin, apiLogout*
> - **app\Http\Controllers\Auth\RegisterController.php** - Handles the user's registration requests and api requests
> 	- *functions:*
> 		- *validator, create, apiRegister, registered*
> - **app\Http\Controllers\HomeController.php** - Shows the main logged screen where the agent can see their assigned contacts and assing unassigned contacts
> 	- *functions:*
> 		- *home, assigns*
> - **app\Http\Controllers\ContactController.php** - Handles all the contact api where there can be created and modified contacts
> 	- *functions:*
> 		- *index, show, store, update, delete, assign*
> 	- *note:* this controller cannot be accessed without login information prevented by routes middleware.
