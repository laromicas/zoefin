<?php

use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDistance()
    {
        $distance = calculate_distance([45.049288,-70.268762], [44.807906, -70.10984]);
        $this->assertEquals($distance, 18.399844448346066);
        $distance = calculate_distance([45.049288,-70.268762], [44.807906, -70.10984], 'K');
        $this->assertEquals($distance, 18.399844448346066 * 1.609344);
    }
}
