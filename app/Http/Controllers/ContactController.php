<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\User;

class ContactController extends Controller
{
    public function index()
    {
        return Contact::all();
    }

    public function show(Contact $contact)
    {
        return $contact;
    }

    public function store(Request $request)
    {
        $contact = Contact::create($request->all());

        return response()->json($contact, 201);
    }

    public function update(Request $request, Contact $contact)
    {
        $contact->update($request->all());

        return response()->json($contact, 200);
    }

    public function delete(Contact $contact)
    {
        $contact->delete();

        return response()->json(null, 204);
    }

    public function assign(Request $request)
    {
        foreach ($request->input('user') as $user_key => $user_input) {
            $user = User::find($user_key);
            $user->update($user_input);
        }
        $users = User::whereNotNull('zipcode_id')->get();
        $users->load('zipcode');
        $contacts = Contact::where('user_id', null)->get();
        $contacts->load('zipcode');
        foreach ($contacts as $contact) {
            if(!($contact->zipcode->latitude ?? null)) {
                $closest = null;
                foreach ($users as $user) {
                    if ($closest === null || abs(intval($contact->zipcode_id) - $closest->zipcode_id) > abs(intval($user->zipcode_id) - intval($contact->zipcode_id))) {
                        $closest = $user;
                    }
                }
            } else {
                $closest = null;
                $distance = 999999999999999;
                $point1 = [$contact->zipcode->latitude, $contact->zipcode->longitude];
                foreach ($users as $user) {
                    $point2 = [$user->zipcode->latitude, $user->zipcode->longitude];
                    $new_distance = calculate_distance($point1, $point2);
                    if ($distance > $new_distance) {
                        $closest = $user;
                        $distance = $new_distance;
                    }
                }
            }
            $contact->user_id = $closest->id;
            $contact->save();
        }
        if($request->wantsJson()) {
            return response()->json([
                'data' => 'Contacts assigned!'
            ], 200);
        } else {
            $request->session()->flash('message', 'Contacts assigned!');
            return redirect()->route('assigns');
        }
    }
}