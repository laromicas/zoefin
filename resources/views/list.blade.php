@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Contacts</div>
                <div class="card-body">
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                    <h5>My Clients:</h5>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Agent Id</th>
                                <th>Contact Name</th>
                                <th>Zip Code</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contacts as $contact)
                            <tr>
                                <th>{{ $contact->user_id }}</th>
                                <th>{{ $contact->name }}</th>
                                <th>{{ $contact->zipcode_id }}</th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <form action="/contacts/assign" method="POST">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">Assign Contacts</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
