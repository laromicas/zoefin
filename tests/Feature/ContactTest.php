<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Contact;
use App\User;

class ContactTest extends TestCase
{
    public function testsContactsAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload = [
            'name' => 'Lorena',
            'zipcode_id' => '33142',
        ];

        $this->json('POST', '/api/contacts', $payload, $headers)
            ->assertStatus(201)
            ->assertJson(['name' => 'Lorena', 'zipcode_id' => '33142']);
    }

    public function testsContactsAreUpdatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $article = factory(Contact::class)->create([
            'name' => 'Bobbit',
            'zipcode_id' => '45342',
        ]);

        $payload = [
            'name' => 'Lorena',
            'zipcode_id' => '33142',
        ];

        $response = $this->json('PUT', '/api/contacts/' . $article->id, $payload, $headers)
            ->assertStatus(200)
            ->assertJson([
                'name' => 'Lorena',
                'zipcode_id' => '33142'
            ]);
    }

    public function testsContactsAreDeletedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $article = factory(Contact::class)->create([
            'name' => 'Bobbit',
            'zipcode_id' => '45342',
        ]);

        $this->json('DELETE', '/api/contacts/' . $article->id, [], $headers)
            ->assertStatus(204);
    }

    public function testContactsAreListedCorrectly()
    {
        factory(Contact::class)->create([
            'name' => 'Bobbit',
            'zipcode_id' => '45342',
        ]);

        factory(Contact::class)->create([
            'name' => 'Lorena',
            'zipcode_id' => '33142',
        ]);

        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/contacts', [], $headers)
            ->assertStatus(200)
            ->assertJson([
                [ 'name' => 'Bobbit', 'zipcode_id' => '45342' ],
                [ 'name' => 'Lorena', 'zipcode_id' => '33142' ]
            ])
            ->assertJsonStructure([
                '*' => ['id', 'name', 'zipcode_id', 'user_id', 'created_at', 'updated_at'],
            ]);
    }


    public function testContactsAssign()
    {
        factory(Contact::class)->create([
            'name' => 'Bobbit',
            'zipcode_id' => '45342',
        ]);

        factory(Contact::class)->create([
            'name' => 'Lorena',
            'zipcode_id' => '33142',
        ]);

        factory(User::class)->create([
            'name' => 'Agent 1',
            'email' => 'agent1@zoefin.com',
            'password' => bcrypt('test123'),
            'zipcode_id' => '33016',
        ]);

        factory(User::class)->create([
            'name' => 'Agent 2',
            'email' => 'agent2@zoefin.com',
            'password' => bcrypt('test123'),
            'zipcode_id' => '84726',
        ]);

        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $payload = [
            'user' => [
                1 => [
                    'id' => 1,
                    'zipcode_id' => 33142
                ],
                2 => [
                    'id' => 2,
                    'zipcode_id' => 86307
                ],
            ]
        ];

        $response = $this->json('POST', '/api/contacts/assign', $payload, $headers)
            ->assertStatus(200)->assertJson([
                'data' => 'Contacts assigned!'
            ]);

        $contacts = Contact::all();
        foreach ($contacts as $contact) {
            $this->assertNotNull($contact->user_id);
        }

        $user = User::find(1);
        $this->assertEquals($user->zipcode_id, 33142);
    }
}