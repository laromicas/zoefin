<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['name', 'user_id', 'zipcode_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function zipcode()
    {
        return $this->belongsTo('App\Zipcode');
    }
}
