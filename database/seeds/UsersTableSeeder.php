<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        // And now, let's create a few articles in our database:
        User::create([
            'name' => 'Agent 1',
            'email' => 'agent1@zoefin.com',
            'password' => bcrypt('test123'),
            'zipcode_id' => '33016',
        ]);
        User::create([
            'name' => 'Agent 2',
            'email' => 'agent2@zoefin.com',
            'password' => bcrypt('test123'),
            'zipcode_id' => '84726',
        ]);
    }
}
